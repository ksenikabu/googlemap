var geocoder;
var map;
var records = [];
var dost;


function initialize() {

    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(55.039, 89.928);
    var myOptions = {
        zoom: 16,
        center: latlng,
        mapTypeControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },
        zoomControlOptions: {
            position: google.maps.ControlPosition.BOTTOM_LEFT,
            style: google.maps.ZoomControlStyle.SMALL
        },
        panControl: false,
        disableDoubleClickZoom: true,
        scrollwheel: false,
        scaleControl: true,
        scaleControlOprions: {
            position: google.maps.ControlPosition.TOP_LEFT
        },

        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);


}

function validate_form() {

    if (document.map.address.value === "") {

        alert("please..");

    } else {

        initialize();
        codeAddress();
        initStorage1();
    }
}

function codeAddress() {

    var address = document.getElementById("address").value;
    geocoder.geocode({
        'address': address
    }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);

            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
            var defaultBounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(),
                new google.maps.LatLng());
            //    map.fitBounds(defaultBounds);

            var input = document.getElementById('address');
            var searchBox = new google.maps.places.SearchBox(input);

            google.maps.event.addListener(searchBox, 'submit', function() {
                var place = searchBox.getPlaces()[0];

                if (!place.geometry) return;
                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }
            });
            google.maps.event.addListener(map, 'bounds_changed', function() {
                var bounds = map.getBounds();
                searchBox.setBounds(bounds);
            });
        } else {
            // alert("Can not find because of " + status);
        }
    });
}

function checkName(field) {
    label = document.getElementById(field.id + "mes");
    if (isEmpty(field)) {
        label.innerHTML = "<span class='right'>put smth!</span>";
        dost = false;
    } else if (!isNameCorrect(field)) {
        label.innerHTML = "<span class='right'>nope!</span>";
        dost = false;
    } else if (isLonger20(field)) {
        label.innerHTML = "<span class='right'>no longer than 20!</span>";
        dost = false;
    } else if (containsDigits(field)) {
        label.innerHTML = "<span class='right'>no numbers!</span>";
        dost = false;
    } else {
        label.innerHTML = "<span class='wrong'>ok</span>";

    }

}

function isNameCorrect(field) {
    var NameRegex = /([A-Z]|[a-z])\w+/;
    return NameRegex.test(field.value);
}

function isEmpty(field) {
    return field.value.length === 0;
}

function isLonger20(field) {
    return field.value.length > 20;
}

function containsDigits(field) {
    var digits = "0123456789";
    for (var i = 0; i < digits.length; i++) {
        if (field.value.indexOf(digits[i]) != -1) {
            return true;
        }
    }
    return false;
}

function checkEmail(field) {
    label = document.getElementById(field.id + "mes");
    if (isEmpty(field)) {
        label.innerHTML = "<span class='right'>put smth!</span>";
        dost = false;
    } else if (!isCorrectEmail(field)) {
        label.innerHTML = "<span class='right'>it's not correct e-mail!</span>";
        dost = false;
    } else {
        label.innerHTML = "<span class='wrong'>ok</span>";
    }
}

function isCorrectEmail(field) {
    var emailRegex = /^\w+@\w+\.\w{2,4}$/;
    return emailRegex.test(field.value);
}

function checkPhone(field) {
    label = document.getElementById(field.id + "mes");
    if (isEmpty(field)) {
        label.innerHTML = "<span class='right'>put smth!</span>";
        dost = false;
    } else if (!isCorrectPhone(field)) {
        label.innerHTML = "<span class='right'>it's not correct!</span>";
        dost = false;
    } else {
        label.innerHTML = "<span class='wrong'>ok</span>";
    }
}

function isCorrectPhone(field) {
    var phoneRegex = /^359\d{9}$/;
    return phoneRegex.test(field.value);
}

function checkAddress(field) {
    label = document.getElementById(field.id + "mes");
    if (isEmpty(field)) {
        label.innerHTML = "<span class='right'>put smth!</span>";
        dost = false;
    } else {
        label.innerHTML = "<span class='wrong'>ok</span>";
    }
}

function openNewMap() {
    var myLatlng = new google.maps.LatLng(55.018803, 82.933952);
    var geocoder = new google.maps.Geocoder();
    map = new google.maps.Map(document.getElementById('map_canvas'), {
        center: myLatlng,
        zoom: 15
    });
    marker = new google.maps.Marker({
        map: map,
        position: myLatlng,
        draggable: true
    });
    //  google.maps.event.addListener(map, 'click', addMarker);

    geocoder.geocode({
        'latLng': myLatlng
    }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                $('#address').val(results[0].formatted_address);

                infowindow.setContent(results[0].formatted_address);
                infowindow.open(map, marker);
            }
        }
    });


    google.maps.event.addListener(marker, 'dragend', function() {

        geocoder.geocode({
            'latLng': marker.getPosition()
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $('#address').val(results[0].formatted_address);

                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                }
            }
        });
    });




}

function addMarker(event) {
    var marker = new google.maps.Marker({
        position: event.latLng,
        map: map,
        //    draggable: true,

        icon: "http://maps.google.com/mapfiles/ms/micons/yellow-dot.png"
    });

    google.maps.event.addListener(marker, 'click', function() {
        marker.setMap(null);
    });
}

function send() {

    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var phone = document.getElementById("phone").value;
    var address = document.getElementById("address").value;

    var r = new Record(name, email, phone, address);
    records.push(r);

}

function checkAll() {
    dost = true;
    checkName(document.getElementById("name"));
    checkEmail(document.getElementById("email"));
    checkPhone(document.getElementById("phone"));
    checkAddress(document.getElementById("address"));



    if (dost === true) {
        enable();
    } else {
        disable();
    }


}

function enable() {
    document.getElementById("submit").disabled = false;
}

function disable() {
    document.getElementById("submit").disabled = true;
}

function initStorage() {

    function saveName() {
        var name = document.getElementById('name');
        localStorage.setItem('name', name.value);
    }

    function savePhone() {
        var name = document.getElementById('phone');
        localStorage.setItem('phone', phone.value);
    }

    function saveEmail() {
        var name = document.getElementById('email');
        localStorage.setItem('email', email.value);
    }

    saveName();
    savePhone();
    saveEmail();

}

function initStorage1() {
    var data = {
        name: document.getElementById('name').value,
        phone: document.getElementById('phone').value,
        email: document.getElementById('email').value,
    };
    data = JSON.stringify(data);
    localStorage.setItem('info', data);

}
